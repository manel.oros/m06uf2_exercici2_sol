package main;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import com.github.javafaker.Faker;
import java.util.Random;

import model.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/***
 * 
 * @author Manel, Adrian Garcia Gómez
 */
public class Main {

    private static org.hibernate.SessionFactory sessionFactory;
    private static Session session;
    private static final Logger logger = LogManager.getLogger(Main.class);
    private static Random rnd;
    
    public static void main(String[] args) {
        
        try {
            
            logger.info("Preparem hibernate:");
            sessionFactory = new Configuration().configure("hibernateConfig/hibernate.cfg.xml").buildSessionFactory();
            session = sessionFactory.openSession();
            session.getTransaction().begin();
            rnd = new Random();
            
            logger.info("Preparem generador de dades simulades:");
            Faker faker = new Faker();

            logger.info("Inserim registres:");
            for (int i = 0; i < 1000; i++) {
                String id = String.valueOf(i);

                Customer customer = new Customer(
                    String.valueOf(i),
                    faker.name().firstName(),
                    faker.name().firstName(),
                    faker.name().lastName(),
                    faker.phoneNumber().cellPhone(),
                    faker.address().city(),
                    faker.address().city(),
                    faker.address().city(),
                    faker.address().state(),
                    faker.country().countryCode2(),
                    faker.country().countryCode2(), i,
                    rnd.nextFloat()*100
                );

                logger.info("Els persistim tots alhora:");
                session.persist(customer);
            }

            logger.info("Els desem tots alhora:");
            session.getTransaction().commit();

        } catch (HibernateException ex) {
            if (session.getTransaction()!=null) 
                session.getTransaction().rollback();
            
            logger.error("Excepció d'hibernate: " + ex.getMessage());
      } catch (NumberFormatException ex){
             if (session.getTransaction() !=null) 
                 session.getTransaction().rollback();
             logger.error("Error amb el format numèric: " + ex.getMessage());
        }
        finally {
            
        logger.info("Tanquem connexió a BBDD:");
        session.close();
         
        logger.info("Parem Hibernate");
        sessionFactory.close();
      }
    }
}
