package model;

public class Customer {
    
    
    public Customer(
    String dni, 
    String customerName, 
    String contactLastName,
    String contactFirstName, 
    String phone, 
    String addressLine1,
    String addressLine2, 
    String city, 
    String state, 
    String postalCode,
    String country, 
    Integer salesRepEmployeeNumber, 
    Float creditLimit)   
    {
        this.dni = dni;
        this.customerName = customerName;
        this.contactLastName = contactLastName;
        this.contactFirstName = contactFirstName;
        this.phone = phone;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
        this.salesRepEmployeeNumber = salesRepEmployeeNumber;
        this.creditLimit = creditLimit;
    }
    
    
    public Customer() {}

    
    public Customer(Integer idCustomer) {
        this.idCustomer = idCustomer;
    }
    
    private Integer idCustomer;
    public Integer getIdCustomer() { return idCustomer; }
    public void setIdCustomer(Integer idCustomer) { this.idCustomer = idCustomer; }
    
    private String dni;
    public String getDni() { return dni; }
    public void setDni(String dni) { this.dni = dni; }
    
    private String customerName;
    public String getCustomerName() { return customerName; }
    public void setCustomerName(String customerName) { this.customerName = customerName; }
    
    private String contactLastName;
    public String getContactLastName() { return contactLastName; }
    public void setContactLastName(String contactLastName) { this.contactLastName = contactLastName; }
    
    private String contactFirstName;
    public String getContactFirstName() { return contactFirstName; }
    public void setContactFirstName(String contactFirstName) { this.contactFirstName = contactFirstName; }
    
    private String phone;
    public String getPhone() { return phone; }
    public void setPhone(String phone) { this.phone = phone; }
    
    private String addressLine1;
    public String getAddressLine1() { return addressLine1; }
    public void setAddressLine1(String addressLine1) { this.addressLine1 = addressLine1; }
    
    private String addressLine2;
    public String getAddressLine2() { return addressLine2; }
    public void setAddressLine2(String addressLine2) { this.addressLine2 = addressLine2; }
    
    private String city;
    public String getCity() { return city; }
    public void setCity(String city) { this.city = city; }
    
    private String state;
    public String getState() { return state; }
    public void setState(String state) { this.state = state; }
    
    private String postalCode;
    public String getPostalCode() { return postalCode; }
    public void setPostalCode(String postalCode) { this.postalCode = postalCode; }
    
    private String country;
    public String getCountry() { return country; }
    public void setCountry(String country) { this.country = country; }
    
    private Integer salesRepEmployeeNumber;
    public Integer getSalesRepEmployeeNumber() { return salesRepEmployeeNumber; }
    public void setSalesRepEmployeeNumber(Integer salesRepEmployeeNumber) { this.salesRepEmployeeNumber = salesRepEmployeeNumber; }
    
    private Float creditLimit;
    public Float getCreditLimit() { return creditLimit; }
    public void setCreditLimit(Float creditLimit) { this.creditLimit = creditLimit; }
}
